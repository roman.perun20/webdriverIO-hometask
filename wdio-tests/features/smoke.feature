Feature: The Trello Website

Scenario Outline: Check "Sign in" functionality
  Given the User is on the home page
  And the User checks the visibility of the Log in button

  When the User clicks on the Log in button
  And the User enters the username and clicks on the Continue button
  And the User enters the password and clicks on the Login button

  Then the current URL contains the <accountName>

  Examples:
      | accountName      |
      | romanperun20111  |


Scenario Outline: Edit user profile information
  Given User is logged in to Trello
  When User navigates to the manage profile page
  And User updates the email to <email> and saves the changes
  Then User should see a info <message> 
  And User clicks cancel email changes and closes the tab

  Examples:
      | email              | message                     |
      | test1234@email.com | Update pending verification |


Scenario Outline: User creates a new board on Trello
  Given User is on the Trello main page
  When User clicks on the Create button
  And User clicks on the Create Board button
  And User enters the board name as <boardName> and clicks the Save button
  Then User checks that the new board <boardName> is successfully created

  Examples:
      | boardName   |
      | new board   |


Scenario Outline: User search for a board
  Given User is on the Trello board page
  When User clicks on the Search input
  And User enter the board name <boardName> and selects one from the list
  Then User is navigated to selected <boardName> page

  Examples:
      | boardName   |
      | new board   |

Scenario: User creates a new list on a board
  Given User is on the opened board page
  When User adds the following list to the board:
    | List Name  |
    | New List   |
  Then User checks that list was created
    | List Name  |
    | New List   |

Scenario Outline: User create new card in the list
  Given User is on the opened list on board page
  When User clicks on add card input
  And User enter the card title <cardTitle> and click Add card button
  Then User check that card <cardTitle> was created

  Examples:
      | cardTitle   |
      | new card    |


Scenario Outline: User check filter cards functionality
  Given User is on board page with created cards
  When User clicks on filters button
  And User enter the search keyword <keyword>
  Then User checks results message <message> 

  Examples:
      | keyword     | message                |
      | new card    | 1 card matches filters |


Scenario Outline: Edit Workspace Name
  Given User is on the board page
  When User clicks on the workspace setting button
  And User chooses a workspace setting option
  When User clicks on the edit workspace button
  And User edits workspace name to <newName> and clicks on Save button
  Then User checks for the edited workspace name: <newName>

  Examples:
      | newName          | 
      | New Workspace    | 