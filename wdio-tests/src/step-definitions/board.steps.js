const { Given, When, Then } = require("@wdio/cucumber-framework");
const { browser } = require("@wdio/globals");
const { assert } = require("chai");

const { BoardPage } = require("../support/const/pages.const");

const pages = {
  board: BoardPage,
};

//Scenario Outline: User creates new list on a board
Given(/^User is on the opened board page$/, async () => {
  browser.maximizeWindow();
  await browser.waitUntil(async () => await pages.board.createdBoardName.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "Board page is not displayed",
  });
});

When("User adds the following list to the board:", async (dataTable) => {
  const listName = dataTable.raw().slice(1);
  await pages.board.clickAddListInput();
  await pages.board.fillIntoTextArea(listName[0][0]);
  await pages.board.addNewListBtnClick();
});

Then("User checks that list was created", async (dataTable) => {
  const listItem = await (await pages.board.firstListItem).getText();
  const listName = dataTable.raw().slice(1)[0][0];
  assert.equal(listItem, listName);
});

//Scenario Outline: User create new card in the list
Given(/^User is on the opened list on board page$/, async () => {
  browser.maximizeWindow();
  await browser.pause(5000);
});

When(/^User clicks on add card input$/, async () => {
  await pages.board.clickAddCardInput();
});

When(/^User enter the card title (.*) and click Add card button$/, async (cardTitle) => {
  await pages.board.fillIntoCardText(cardTitle);
  await browser.pause(2000);
  await pages.board.clickAddCardBtn();
});

Then(/^User check that card (.*) was created$/, async (cardTitle) => {
  const createdCardTitle = await (await pages.board.createdCardTitle).getText();
  assert.equal(createdCardTitle, cardTitle);
});

//Scenario Outline: User check filter cards functionality
Given(/^User is on board page with created cards$/, async () => {
  browser.maximizeWindow();
  await browser.waitUntil(() => pages.board.filterBtn.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "Filter button is not displayed",
  });
});

When(/^User clicks on filters button$/, async () => {
  await pages.board.clickFilterBtn();
});

When(/^User enter the search keyword (.*)$/, async (keyword) => {
  await pages.board.fillFilterPlaceholderInput(keyword);
});

Then(/^User checks results message (.*)$/, async (message) => {
  const filterResultMessage = await (await pages.board.filterResultMessage).getText();
  assert.equal(filterResultMessage, message);
});
