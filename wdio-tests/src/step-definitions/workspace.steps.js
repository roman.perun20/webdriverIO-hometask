const { Given, When, Then } = require("@wdio/cucumber-framework");
const { browser } = require("@wdio/globals");
const { expect } = require("chai");

const { WorkspacePage } = require("../support/const/pages.const");

const pages = {
  workspace: WorkspacePage,
};

//Scenario Outline: Edit Workspace Name
Given(/^User is on the board page$/, async () => {
  browser.maximizeWindow();
  await browser.pause(5000);
});

When(/^User clicks on the workspace setting button$/, async () => {
  await pages.workspace.clickWorkspaceBtn();
});

When(/^User chooses a workspace setting option$/, async () => {
  await pages.workspace.clickWorkspaceOption();
});

When(/^User clicks on the edit workspace button$/, async () => {
  await pages.workspace.clickEditWorkspaceBtn();
});

When(/^User edits workspace name to (.*) and clicks on Save button$/, async (newName) => {
  await pages.workspace.editWorkspaceName(newName);
  await pages.workspace.submitBtnClick();
});

Then(/^User checks for the edited workspace name: (.*)$/, async (newName) => {
  const editedWorkspaceTitle = await pages.workspace.editedWorkspaceTitle.getText();
  expect(editedWorkspaceTitle).to.equal(newName);
});
