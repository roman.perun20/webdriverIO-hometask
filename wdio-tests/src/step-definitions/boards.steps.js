const { Given, When, Then } = require("@wdio/cucumber-framework");
const { browser } = require("@wdio/globals");

const chai = require("chai");
const expect = chai.expect;
chai.should();

const { BoardsPage } = require("../support/const/pages.const");

const pages = {
  boards: BoardsPage,
};

// //Scenario: User creates a new board on Trello
Given(/^User is on the Trello main page$/, async () => {
  await browser.maximizeWindow();

  const contentHeader = await pages.boards.contentHeader.getText();
  contentHeader.should.equal("Most popular templates");
});

When(/^User clicks on the Create button$/, async () => {
  await pages.boards.clickCreateMenuBtn();
  await browser.waitUntil(() => pages.boards.createBoardBtn.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "Create board button is not displayed",
  });
});

When(/^User clicks on the Create Board button$/, async () => {
  await pages.boards.clickCreateBoardBtn();
  await browser.pause(2000);
});

When(/^User enters the board name as (.*) and clicks the Save button$/, async (boardName) => {
  await pages.boards.createNewBoard(boardName);
});

Then(/^User checks that the new board (.*) is successfully created$/, async (boardName) => {
  const createdBoardName = await pages.boards.createdBoardName.getText();
  expect(createdBoardName).to.equal(boardName);
});

//Scenario Outline: User search for a board
Given(/^User is on the Trello board page$/, async () => {
  await pages.boards.clickWorkspaces();
  browser.maximizeWindow();
});

When(/^User clicks on the Search input$/, async () => {
  await pages.boards.clickSearchInput();
});

When(/^User enter the board name (.*) and selects one from the list$/, async (boardName) => {
  await pages.boards.searchBoard(boardName);
  await browser.waitUntil(() => pages.boards.createdBoardName.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "New created board name is not displayed",
  });
});

Then(/^User is navigated to selected (.*) page$/, async (boardName) => {
  const createdBoardName = await pages.boards.createdBoardName.getText();
  expect(createdBoardName).to.equal(boardName);
});
