const { Given, When, Then } = require("@wdio/cucumber-framework");
const { browser } = require("@wdio/globals");
const { expect } = require("chai");

const { BoardsPage, ManageProfilePage } = require("../support/const/pages.const");

const pages = {
  boards: BoardsPage,
  profile: ManageProfilePage,
};

// Scenario Outline: Edit user profile information
Given(/^User is logged in to Trello$/, async () => {
  browser.maximizeWindow();
});

When(/^User navigates to the manage profile page$/, async () => {
  await browser.setTimeout({ implicit: 1000 });
  await pages.boards.clickMemberMenuBtn();
  await pages.boards.clickManageAccountLink();

  await browser.switchWindow("id.atlassian");
});

When(/^User updates the email to (.*) and saves the changes$/, async (email) => {
  await pages.profile.clickEmail();
  await pages.profile.fillEmail(email);
  await pages.profile.saveChanges();
});

Then(/^User should see a info (.*)$/, async (message) => {
  const infoText = await pages.profile.infoMessage.getText();
  expect(infoText).to.equal(message);
});

Then(/^User clicks cancel email changes and closes the tab$/, async () => {
  await pages.profile.undoEmailChanges();
  await browser.waitUntil(() => pages.profile.newEmailPlaceholder.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "Input Password is not displayed",
  });
  await browser.closeWindow();
  await browser.switchWindow("trello.com");
});
