const { Given, When, Then } = require("@wdio/cucumber-framework");
const { browser } = require("@wdio/globals");

const { expect } = require("chai");

const { LoginPage, HomePage } = require("../support/const/pages.const");

const pages = {
  login: LoginPage,
  home: HomePage,
};

// Scenario Outline: Check "Sign in" functionality
Given(/^the User is on the home page$/, async () => {
  await pages.home.open();
  await browser.maximizeWindow();
});

Then(/^the User checks the visibility of the Log in button$/, async () => {
  const loginBtn = await pages.home.loginBtn;
  await loginBtn.waitForDisplayed({ timeout: 5000 });
  expect(await loginBtn.isDisplayed(), "Login button should be displayed").to.be.true;
});

When(/^the User clicks on the Log in button$/, async () => {
  await pages.home.clickLoginBtn();
});

When(/^the User enters the username and clicks on the Continue button$/, async () => {
  const username = process.env.USERNAME;
  await pages.login.fillUsername(username);
});

When(/^the User enters the password and clicks on the Login button$/, async () => {
  const password = process.env.PASSWORD;
  await browser.waitUntil(() => pages.login.inputPassword.isDisplayed(), {
    timeout: 5000,
    timeoutMsg: "Input Password is not displayed",
  });
  await pages.login.fillPassword(password);
});

Then(/^the current URL contains the (.*)$/, async (accountName) => {
  await browser.waitUntil(
    async () => {
      const currentUrl = await browser.getUrl();
      return currentUrl.includes(accountName);
    },
    {
      timeout: 5000,
      timeoutMsg: "URL did not contain the expected keyword",
    },
  );
});
