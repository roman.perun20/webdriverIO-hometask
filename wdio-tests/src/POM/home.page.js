const { $ } = require("@wdio/globals");
const Page = require("./page");

class HomePage extends Page {
  get loginBtn() {
    return $('//a[@data-uuid="MJFtCCgVhXrVl7v9HA7EH_login"]');
  }

  async clickLoginBtn() {
    await this.loginBtn.click();
  }

  async open() {
    await super.open("/home");
  }
}

module.exports = new HomePage();
