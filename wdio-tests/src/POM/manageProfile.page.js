const { $ } = require("@wdio/globals");
const Page = require("./page");

class ManageProfilePage extends Page {
  get emailLink() {
    return $('//span[contains(text(), "Email")]/parent::a');
  }

  get saveChangesBtn() {
    return $('//button[@type="submit"]');
  }

  get infoMessage() {
    return $("h2");
  }

  get undoChangeBtn() {
    return $('//button//span[contains(text(),"Undo email change")]');
  }

  get newEmailPlaceholder() {
    return $('//input[@placeholder="Enter new email address"]');
  }

  async clickEmail() {
    await this.emailLink.click();
  }

  async fillEmail(value) {
    const emailElememnt = await $('//input[@placeholder="Enter new email address"]');
    await emailElememnt.addValue(value);
  }

  async saveChanges() {
    await this.saveChangesBtn.click();
  }

  async undoEmailChanges() {
    await this.undoChangeBtn.click();
  }
}

module.exports = new ManageProfilePage();
