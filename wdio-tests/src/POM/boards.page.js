const { $ } = require("@wdio/globals");
const Page = require("./page");

class BoardsPage extends Page {
  get memberMenuBtn() {
    return $('//button[@data-testid="header-member-menu-button"]');
  }

  get manageAccountLink() {
    return $('//a[@data-testid="manage-account-link"]');
  }

  get createMenuBtn() {
    return $('//button[@data-testid="header-create-menu-button"]');
  }

  get createBoardBtn() {
    return $('//button[@data-testid="header-create-board-button"]');
  }

  get inputBoardTitle() {
    return $('//input[@data-testid="create-board-title-input"]');
  }

  get boardSubmitBtn() {
    return $('//button[@data-testid="create-board-submit-button"]');
  }

  get createdBoardName() {
    return $('//h1[@data-testid="board-name-display"]');
  }

  get trelloWorkspaces() {
    return $$('//a[@href="/w/userworkspace13432816"]');
  }

  get searchInput() {
    return $("#search");
  }

  get boardFromSearch() {
    return $$('//a[@class="board-tile mod-light-background"]');
  }

  get contentHeader() {
    return $('//h2[contains(text(), "Most popular templates")]');
  }

  async clickMemberMenuBtn() {
    await this.memberMenuBtn.click();
  }

  async clickManageAccountLink() {
    await this.manageAccountLink.click();
  }

  async clickCreateMenuBtn() {
    await this.createMenuBtn.click();
  }

  async clickCreateBoardBtn() {
    await this.createBoardBtn.click();
  }

  async createNewBoard(value) {
    await this.inputBoardTitle.setValue(value);
    await this.boardSubmitBtn.click();
  }
  async clickWorkspaces() {
    const trelloWorkspace = await this.trelloWorkspaces[1];
    await trelloWorkspace.click();
  }
  async clickSearchInput() {
    await this.searchInput.click();
  }

  async searchBoard(value) {
    await this.searchInput.setValue(value);
    await this.boardFromSearch[0].click();
  }

  async open() {
    await super.open("/");
  }
}

module.exports = new BoardsPage();
