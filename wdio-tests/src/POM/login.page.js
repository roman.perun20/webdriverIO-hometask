const { $ } = require("@wdio/globals");
const Page = require("./page");

class LoginPage extends Page {
  get inputUsername() {
    return $("#username");
  }

  get inputPassword() {
    return $("#password");
  }

  get continueBtn() {
    return $("#login-submit");
  }

  get loginButton() {
    return $("#login-submit");
  }

  async fillUsername(username) {
    await this.inputUsername.setValue(username);
    await this.continueBtn.click();
  }

  async fillPassword(password) {
    await this.inputPassword.setValue(password);
    await this.loginButton.click();
  }
}

module.exports = new LoginPage();
