const { $ } = require("@wdio/globals");
const Page = require("./page");

class WorkspacePage extends Page {
  get workspaceSettingsBtn() {
    return $('//button[@data-testid="admin-settings-dropdown-button"]');
  }

  get workspaceSettingsOption() {
    return $('//a//span[contains(text(), "Workspace settings")]');
  }

  get editWorkspaceBtn() {
    return $('//h2[contains(text(),"New Workspace")]//button');
  }

  get workspaceNameInput() {
    return $("#displayName");
  }

  get submitBtn() {
    return $('//button[contains(text(), "Save")]');
  }

  get editedWorkspaceTitle() {
    return $('//span[@data-testid="EditIcon"]/parent::span/parent::button/parent::h2');
  }

  async clickWorkspaceBtn() {
    await this.workspaceSettingsBtn.click();
  }

  async clickWorkspaceOption() {
    await this.workspaceSettingsOption.click();
  }

  async clickEditWorkspaceBtn() {
    await this.editWorkspaceBtn.click();
  }

  async editWorkspaceName(value) {
    await this.workspaceNameInput.clearValue();
    await this.workspaceNameInput.setValue(value);
  }

  async submitBtnClick() {
    await this.submitBtn.click();
  }
}

module.exports = new WorkspacePage();
