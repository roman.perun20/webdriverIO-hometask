const { $ } = require("@wdio/globals");
const Page = require("./page");

class BoardPage extends Page {
  get createdBoardName() {
    return $('//h1[@data-testid="board-name-display"]');
  }

  //List
  get addListInput() {
    return $('//button[@data-testid="list-composer-button"]');
  }

  get textAreaList() {
    return $("//form//textarea");
  }
  get addListBtn() {
    return $('//button[@data-testid="list-composer-add-list-button"]');
  }

  get firstListItem() {
    return $('//h2[@data-testid="list-name"][1]');
  }

  /// Cards
  get addCardInput() {
    return $('//button[@data-testid="list-add-card-button"]');
  }

  get addTextToCard() {
    return $('//form//textarea[@data-testid="list-card-composer-textarea"]');
  }

  get addCardBtn() {
    return $('//button[@data-testid="list-card-composer-add-card-button"]');
  }

  get createdCardTitle() {
    return $('//a[@data-testid="card-name"]');
  }

  /// Filter cards
  get filterBtn() {
    return $('//button[@data-testid="filter-popover-button"]');
  }

  get filterPlaceholderInput() {
    return $('//input[@placeholder="Enter a keyword…"]');
  }

  get filterResultMessage() {
    return $('//p[contains(text(), "matches filters")]');
  }

  //List methods
  async clickAddListInput() {
    await this.addListInput.click();
  }

  async fillIntoTextArea(value) {
    await this.textAreaList.setValue(value);
  }

  async addNewListBtnClick() {
    await this.addListBtn.click();
  }

  //Card methods
  async clickAddCardInput() {
    await this.addCardInput.click();
  }

  async fillIntoCardText(value) {
    await this.addTextToCard.setValue(value);
  }

  async clickAddCardBtn() {
    await this.addCardBtn.click();
  }

  //Filter methods
  async clickFilterBtn() {
    await this.filterBtn.click();
  }

  async fillFilterPlaceholderInput(value) {
    await this.filterPlaceholderInput.click();
    await this.filterPlaceholderInput.setValue(value);
  }
}

module.exports = new BoardPage();
