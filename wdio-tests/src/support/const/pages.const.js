const LoginPage = require("../../POM/login.page");
const HomePage = require("../../POM/home.page");
const BoardPage = require("../../POM/board.page");
const BoardsPage = require("../../POM/boards.page");
const ManageProfilePage = require("../../POM/manageProfile.page");
const WorkspacePage = require("../../POM/workspace.page");

module.exports = {
  LoginPage,
  HomePage,
  BoardPage,
  BoardsPage,
  ManageProfilePage,
  WorkspacePage,
};
