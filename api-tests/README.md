# API Testing of Dropbox appication.

Familiarize with the [Dropbox API](https://www.dropbox.com/developers/documentation/http/documentation) documentation.

Tests created and implemented using Mocha framework, Chai assertion library and Axios promise-based HTTP library.

The task for creating test scenarios is as follows:

Implement at least 3 test scenarios using one of JS libraries like Axios, Got, SuperTest (it will be a plus) or Postman :
· Upload file
· Get File Metadata
· Delete file

Add assertions for headers, status code and body of response

Add script into package.json to run API tests through the CLI (if tests done in Postman use Newman tool)

To run your tests just call:

```bash
npm run test
```

Access token is stored in an .env file!
But to run this test in the next 24 hours, use the current token:
const DROPBOX_ACCESS_TOKEN =
"sl.Bxpxh-C1k8za52bEgR6z1Kr01sJce5DpvHGBQux2DS7sQFHTvCbSsBaIk_qkdu7qcjOvwkOTB5E0CX-BMBUHGzqKqg0IipEGTNsXLXGpOBQXkH0scbTCNQ06Q5WarTka0b-tG_d7_FCF";
