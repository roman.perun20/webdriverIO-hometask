export const apiData = {
  BASE_URL_API: "https://api.dropboxapi.com/2/files/",
  BASE_URL_CONTENT: "https://content.dropboxapi.com/2/files/"
};
