import { postApi, postContent } from "./api-client.js";
import {
  FILE_CONTENT,
  UPLOAD_FILE_HEADERS,
  METADATA_FILE_BODY,
  METADATA_FILE_HEADERS,
  DELETE_FILE_BODY,
  DELETE_FILE_HEADERS
} from "../resources/constans.js";

export const uploadFile = async () => {
  return await postContent("upload", FILE_CONTENT, UPLOAD_FILE_HEADERS);
};

export const metaDataFile = async () => {
  return await postApi("get_metadata", METADATA_FILE_BODY, METADATA_FILE_HEADERS);
};

export const deleteFile = async () => {
  return await postApi("delete_v2", DELETE_FILE_BODY, DELETE_FILE_HEADERS);
};
