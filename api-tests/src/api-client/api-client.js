import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

import { apiData } from "./api-data.js";

const TOKEN = process.env.DROPBOX_ACCESS_TOKEN;
const instanceAPI = axios.create({
  baseURL: apiData.BASE_URL_API,
  timeout: 3000,
  headers: {
    Authorization: `Bearer ${TOKEN}`
  }
});

const instanceContent = axios.create({
  baseURL: apiData.BASE_URL_CONTENT,
  timeout: 3000,
  headers: {
    Authorization: `Bearer ${TOKEN}`
  }
});

export const postApi = async (route, body, header) => {
  return await instanceAPI.post(route, body, header);
};
export const postContent = async (route, body, header) => {
  return await instanceContent.post(route, body, header);
};
