import { expect } from "chai";
import { uploadFile, metaDataFile, deleteFile } from "../api-client/businessFunctions.js";
import { POST_SUCCESS_CODE, FILE_SIZE } from "../resources/constans.js";

describe("Dropbox API Tests", async () => {
  it("Should successfully upload the file", async () => {
    const response = await uploadFile();
    expect(response.headers["content-type"]).equals("application/json");
    expect(response.data.size).equals(FILE_SIZE);
    expect(response.status).equals(POST_SUCCESS_CODE);
  });

  it("should successfully retrieve the file's metadata", async () => {
    const response = await metaDataFile();
    expect(response.headers["content-type"]).equals("application/json");
    expect(response.data.is_downloadable).equals(true);
    expect(response.status).equals(POST_SUCCESS_CODE);
  });

  it("should successfully delete the file", async () => {
    const response = await deleteFile();
    expect(response.data.metadata.name).equals("Matrices.txt");
    expect(response.status).equals(POST_SUCCESS_CODE);
  });
});
