export const FILE_PATH = "/Homework/uploadFile/Matrices.txt";
export const POST_SUCCESS_CODE = 200;
export const FILE_CONTENT = "The File is successfully uploaded!!";
export const FILE_SIZE = 35;

export const UPLOAD_FILE_HEADERS = {
  headers: {
    "Content-Type": "application/octet-stream",
    "Dropbox-API-Arg": JSON.stringify({
      autorename: false,
      mode: "add",
      mute: false,
      path: FILE_PATH,
      strict_conflict: false
    })
  }
};

export const METADATA_FILE_HEADERS = {
  headers: {
    "Content-Type": "application/json"
  }
};

export const METADATA_FILE_BODY = {
  include_deleted: false,
  include_has_explicit_shared_members: false,
  include_media_info: false,
  path: FILE_PATH
};

export const DELETE_FILE_HEADERS = {
  headers: {
    "Content-Type": "application/json"
  }
};

export const DELETE_FILE_BODY = {
  path: FILE_PATH
};
